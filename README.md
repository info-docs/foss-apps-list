# foss-alternatives

a list of common (and less common) opensource software

• [games](#games)

• [social media](#social_media)

• [blogs about opensource](#blogs)

• [online tools](#tools)

• [chromium extensions](#chromium)

• [firefox extensions](#firefox)

• [LIBRE python libraries](#python_libraries)

• [LIBRE node.js modules](#js_modules)

• [text editors](#text_editors)

• [linux distros](#linux_distros)

• [other operating systems](#other_systems)

• [other apps](#other)


### games

- 0 a.d.

- veloren

- minetest

- super tux kart

- tux racer

- shattered / pixel dungeon

- frozen bubble

- unciv / freeciv

- replica island

- heriswap

[go to top](#foss-alternatives)

### social_media

- \[fediverse\]

- - mastodon

- - pixelfed

- - peertube

- - friendica

- - funkwhale

- - gnu social

- - diaspora\*

- - 

- element / matrix

- signal

- wire

- session

- gamejolt

[go to top](#foss-alternatives)

### blogs

[go to top](#foss-alternatives)

### tools

[go to top](#foss-alternatives)

### chromium

- vimium C

- stylus

- dark reader

- ublock origin

- cookie editor 

- miner block

- prokeys

- save in...

- cc search

[go to top](#foss-alternatives)

### firefox

- vimium C

[go to top](#foss-alternatives)

### python_libraries

[go to top](#foss-alternatives)

### js_modules

[go to top](#foss-alternatives)

### text_editors

- vscodium

- vim 

- emacs

- nano

- micro

[go to top](#foss-alternatives)

### linux_distros

- debian

- arch

- nixOS

- slackware

- alpine

- NuTyX

- puppy

- void

- mobian (mobile)

- postmarketOS (mobile)

[go to top](#foss-alternatives)

### other_systems

- free bsd 

- serenityOS

- templeOS / shrineOS

- greenteaOS

- boneOS

- reactOS

- kolibriOS

- morOS

- dahliaOS

- skiftOS

- duckOS

[go to top](#foss-alternatives)

### other

[go to top](#foss-alternatives)
